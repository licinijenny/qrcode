<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="nav.css">
	<link rel="stylesheet" href="formulario.css">
	<title>QR</title>
</head>
<body>

<div class="wave-container">
	<div class="topnav">
	<img src="img/logo.png" alt="Logo">
	<div class="topnav-right">
		<a  href="http://localhost/QR/index.html">Home</a>
		<a class="active" href="http://localhost/QR/codificar.php">Codificador</a>
		<a href="http://localhost/QR/decodificador.php">Decodificador</a>
	</div>
	</div>
	<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 200">
<path fill="#fff" fill-opacity="1" d="M0,96L48,106.7C96,117,192,139,288,154.7C384,171,480,181,576,160C672,139,768,85,864,69.3C960,53,1056,75,1152,96C1248,117,1344,139,1392,149.3L1440,160L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z">
</path>
</svg>




<div class='some-page-wrapper'>
  <div class='row'>
    <div class='column'>
      <div class='blue-column'>
        <h1>OBTENER QR</h1>
		<form id="generador" method="post">
			<input type="text" name="dni" id="dni" placeholder="DNI">
			 <input class="one" type="submit" name="registrar" value="QR CODE">
			<input type="button" value="ACTUALIZAR" onclick="location.reload()"/>
		</form>
      </div>
    </div>
    <div class='column'>
      <div class='green-column'>
        <div class='result'>
		<img class='sfondo' src="img/qr2.png" alt="qr">
		</div>
      </div>
    </div>
  </div>
 </div>
  
  
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script>
		$( "#generador" ).submit(function( event ) {
			var dni=$("#dni").val();
			parametros={"dni":dni};
			 $.ajax({
				type: "POST",
				url: "loginqr.php",
				data: parametros,
				success: function(datos){
					$(".result").html(datos);
				} 
			 })
		  event.preventDefault();
		});
	</script>
</body>
</html>
