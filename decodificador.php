<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

		<link rel="stylesheet" href="nav.css">
		<link rel="stylesheet" href="decodificador.css">
	<title>QR</title>
	
</head>
<body>
<div class="wave-container">
	<div class="topnav">
	<img src="img/logo.png" alt="Logo">
	<div class="topnav-right">
		<a  href="http://localhost/QR/index.html">Home</a>
		<a href="http://localhost/QR/codificar.php">Codificador</a>
		<a class="active" href="#Decodificador">Decodificador</a>
	</div>
</div>
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 200">
<path fill="#fff" fill-opacity="1" d="M0,96L48,106.7C96,117,192,139,288,154.7C384,171,480,181,576,160C672,139,768,85,864,69.3C960,53,1056,75,1152,96C1248,117,1344,139,1392,149.3L1440,160L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z">
</path>
</svg>

	
	<div class='some-page-wrapper'>
  <div class='row'>
    <div class='column'>
      <div class='blue-column'>
        
		
		<div class="container">
		<h1>DECODIFICAR CODIGOS QR</h1>
				<form method="POST" enctype="multipart/form-data" id="fileUploadForm">
				<input type="file" name="qrimage" id="qrimage" class="form-control" required="" ><br>
				<input type="submit" class="btn btn-md btn-block btn-info" value="ENVIAR DATOS" id="btnSubmit"/>	
			</form>		
				
	</div>
      </div>
    </div>
    <div class='column'>
      <div class='green-column'>
	  <div id="result"></div>
       <img src="img/scan.jpg" alt="scan" style="width: 320px;">	
      </div>
    </div>
  </div>
</div>
</div>	
 
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
 

<script>
 $(document).ready(function () {

    $("#btnSubmit").click(function (event) {

        //stop submit the form, we will post it manually.
        event.preventDefault();

        // Get form
        var form = $('#fileUploadForm')[0];

		// Create an FormData object
        var data = new FormData(form);

		// If you want to add an extra field for the FormData
        data.append("CustomField", "This is some extra data, testing");

		// disabled the submit button
        $("#btnSubmit").prop("disabled", true);

        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "decode.php",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data) {

                $("#result").text(data);
                console.log("SUCCESS : ", data);
                $("#btnSubmit").prop("disabled", false);

            },
            error: function (e) {

                $("#result").text(e.responseText);
                console.log("ERROR : ", e);
                $("#btnSubmit").prop("disabled", false);

            }
        });

    });

});
</script>
 


</body>
</html>